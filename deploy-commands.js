module.exports = {
    async deployCommands(clientId, guildIds) {
        const { REST, SlashCommandBuilder, Routes } = require('discord.js');
        const { token, language } = require('./config.json');
        const i18nModule = require("i18n-nodejs");
        const i18n = new i18nModule(language, "./../../translations.json");

        const commands = [
            new SlashCommandBuilder().setName(i18n.__("commandPingName"))
                .setDescription(i18n.__("commandPingDescription")),
            new SlashCommandBuilder().setName(i18n.__("commandInfoName"))
                .setDescription(i18n.__("commandInfoDescription")),
            new SlashCommandBuilder().setName(i18n.__("commandAboutName"))
                .setDescription(i18n.__("commandAboutDescription")),
            new SlashCommandBuilder().setName(i18n.__("commandPlayersName"))
                .setDescription(i18n.__("commandPlayersDescription")),
        ].map(command => command.toJSON());

        const rest = new REST({ version: '10'}).setToken(token);

        if(guildIds.size!==1){
            rest.put(Routes.applicationCommands(clientId),
            { body: commands },
        ).then((data)=>{
            console.log(i18n.__("commandRegistrationSuccessful", {amount: data.length}));
        }).catch(console.error);
        } else {
            rest.put(Routes.applicationGuildCommands(clientId, [...guildIds][0][0]),
                { body: commands }
            ).then((data)=>{
                console.log(i18n.__("commandRegistrationSuccessful", {amount: data.length}));
            }).catch(console.error);
        }
    }
}