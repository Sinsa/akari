﻿const {Client, GatewayIntentBits, EmbedBuilder, InteractionType} = require("discord.js");
const fetch = require('node-fetch');
const { GameDig } = require("gamedig");
const client = new Client({intents: GatewayIntentBits.Guilds});
const DeployCommands = require('./deploy-commands');
let {
    token,
    srvip,
    srvport,
    srvgame,
    aboutdesc,
    srvcollection,
    language,
    commandOutputColor
} = require("./config.json");
const {version, repository} = require("./package.json");
const i18nModule = require("i18n-nodejs");
const i18n = new i18nModule(language, "./../../translations.json");

if (
    token == null ||
    srvip == null ||
    srvgame == null
) {
    console.error(i18n.__("noconfigs"));
    console.error(`token\tsrvip\tsrvgame`);
    console.error(i18n.__("helpInfo"));
    process.exit(1);
}

setDefaultsForMissingConfigs();

client.once("ready", async () => {
    DeployCommands.deployCommands(client.user.id, client.guilds.cache)
    console.log(i18n.__("loginSuccessful", {discordTag: client.user.tag}));
    console.log(`${i18n.__("startupConfirmation")}\n\n`);
    client.user.setActivity(i18n.__("startStatus"));
    client.user.setStatus('idle');
    setInterval(checkOnlinePlayers, 30000);
    setInterval(checkVersion, 86400000);
    checkOnlinePlayers();
    checkVersion();
});

client.on("interactionCreate", async (interaction) => {
    if (interaction.type !== InteractionType.ApplicationCommand) {
        return; // If the interaction is not a chat command its not interesting to us
    }
    await interaction.deferReply();
    const {commandName} = interaction;
    if (commandName === "ping") {
        interaction.editReply(
            `🏓 My current ping is ${Math.round(client.ws.ping)} ms.`
        );
    } else if (commandName === "info") {
        GameDig.query({
            type: srvgame,
            host: srvip,
            port: srvport,
            maxAttempts: 1,
        })
            .then((state) => {
                const reply = new EmbedBuilder()
                    .setTitle(i18n.__("infoCommandTitle", {serverName: state.name}))
                    .setColor(commandOutputColor)
                    .setTimestamp()
                    .setFooter({text: i18n.__("requestor", {user: interaction.user.tag})})
                    .setThumbnail(client.user.avatarURL())
                    .addFields(
                        {
                            name: i18n.__("infoCommandOnlinePlayers"),
                            value: `${state.numplayers}/${state.maxplayers}`,
                            inline: true
                        },
                        {
                            name: i18n.__("infoCommandPing"),
                            value: state.ping.toString()
                        }
                    )
                if (state.map) {
                    reply.addFields({name: i18n.__("infoCommandCurrentMap"), value: state.map, inline: true});
                }
                if (srvcollection) {
                    reply.addFields({
                        name: i18n.__("infoCommandActions"),
                        value: `[${i18n.__("infoCommandOpenCollection")}](https://steamcommunity.com/sharedfiles/filedetails/?id=${srvcollection})`
                    });
                }
                interaction.editReply({embeds: [reply]});
            })
            .catch((error) => {
                console.error(error);
                client.user.setActivity('offline');
                client.user.setStatus('dnd');
            });
    } else if (commandName === "about") {
        try {
            const aboutEmbed = new EmbedBuilder();
            let status;
            switch (client.ws.status) {
                case 0:
                    status = i18n.__("statusReady");
                    break;
                case 1:
                    status = i18n.__("statusConnecting");
                    break;
                case 2:
                    status = i18n.__("statusReconnecting");
                    break;
                case 3:
                    status = i18n.__("statusInactive");
                    break;
                case 4:
                    status = i18n.__("statusAlmostReady");
                    break;
                default:
                    status = i18n.__("statusDisconnected");
            }
            let msleft = client.uptime;
            let dleft = 0;
            let hleft = 0;
            let mleft = 0;
            let sleft = 0;
            while (msleft >= 86400000) {
                msleft -= 86400000;
                dleft++;
            }
            while (msleft >= 3600000) {
                msleft -= 3600000;
                hleft++;
            }
            while (msleft >= 60000) {
                msleft -= 60000;
                mleft++;
            }
            while (msleft >= 1000) {
                msleft -= 1000;
                sleft++;
            }
            aboutdesc = aboutdesc ? "\n\n" + aboutdesc : "";
            aboutEmbed
                .setTitle(i18n.__("aboutTitle", {discordTag: client.user.tag}))
                .setTimestamp()
                .setColor(commandOutputColor)
                .setFooter({text: i18n.__("requestor", {user: interaction.user.tag})})
                .setDescription(i18n.__("aboutDescription", {
                    discordUsername: client.user.username,
                    version: version,
                    aboutme: aboutdesc
                }))
                .addFields({
                    name: "Uptime",
                    value: i18n.__("time", {days: dleft, hours: hleft, minutes: mleft, seconds: sleft}),
                    inline: true
                })
                .addFields({
                    name: "Status",
                    value: status,
                    inline: true
                });
            interaction.editReply({embeds: [aboutEmbed]});
        } catch (e) {
            console.log(e);
        }
    } else if (commandName === "players") {
        GameDig.query({
            type: srvgame,
            host: srvip,
            port: srvport,
            maxAttempts: 1,
        })
            .then((state) => {
                let playerslist = "";
                let pointslist = "";
                if (state.players.length === 0) {
                    playerslist = `\n${i18n.__("playerlistNoPlayersOnline")}`;
                    pointslist = "\n😦";
                } else if (state.players[0].name) {
                    for (let i = 0; i < state.players.length; i++) {
                        if (state.players[i].name) {
                            if (i === 0) {
                                playerslist += `\n${state.players[i].name}\n`;
                                pointslist += `\n${state.players[i].score} 🚩\n`;
                            } else {
                                playerslist += `${state.players[i].name}\n`;
                                pointslist += `${state.players[i].score} 🚩\n`;
                            }
                        }
                    }
                }
                const reply = new EmbedBuilder()
                    .setTitle(i18n.__("playerlistCurrentPlayers"))
                    .setColor(commandOutputColor)
                    .setTimestamp()
                    .setFooter({text: i18n.__("requestor", {user: interaction.user.tag})})
                    .setThumbnail(client.user.avatarURL())
                    .addFields({
                            name: i18n.__("playerlistConnectedPlayers"),
                            value: playerslist,
                            inline: true
                        },
                        {
                            name: i18n.__("playerlistPoints"),
                            value: pointslist,
                            inline: true
                        });
                interaction.editReply({embeds: [reply]});
            })
            .catch((error) => {
                client.user.setActivity('offline');
                client.user.setStatus('dnd');
                console.error(error);
            });
    }
});

client.login(token);

function checkOnlinePlayers() {
    GameDig.query({
        type: srvgame,
        host: srvip,
        port: srvport,
        maxAttempts: 1,
    })
        .then((state) => {
            const date = new Date();
            console.log(
                `[${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}] ${i18n.__("loggingQuery", {
                    ip: srvip,
                    players: `${state.numplayers}/${state.maxplayers}`
                })}`
            );
            client.user.setActivity(`${state.numplayers}/${state.maxplayers}`);
            client.user.setStatus('online');
        })
        .catch(() => {
            client.user.setActivity('offline');
            client.user.setStatus('dnd');
        });
}

async function checkVersion() {
    const publishedVersionData = await fetch("https://gitlab.com/api/v4/projects/25219633/repository/tags").then(response=>response.json());
    const latestAkariVersion = publishedVersionData[0].message;
    if(latestAkariVersion!==version) {
        console.warn("!----------------------IMPORTANT NOTICE----------------------!");
        console.warn(`There is a new version of Akari available! Please head to ${repository.url.substring(0,repository.url.lastIndexOf("."))} and download the newest release.`);
        console.warn();
        console.warn(`Latest released version:\t${latestAkariVersion}`);
        console.warn(`Your current Akari version:\t${version}`);
        console.warn("!------------------------------------------------------------!");
    }
}

function setDefaultsForMissingConfigs() {
    srvport = srvport || 27015;
    commandOutputColor = commandOutputColor || 'Random';
}
