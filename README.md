# Akari

![Latest Stable Version: 2.1.0](https://img.shields.io/badge/version-v2.1.0-blue) ![Language: JavaScript](https://img.shields.io/badge/Language-JavaScript-yellowgreen) ![Framework: Discord.js](https://img.shields.io/badge/Framework-Discord.js-blue)

### About

Akari is a Discord Bot, which shows the current Playercount and some other stats on any Source based Gameserver!

### Lead Developer

Sinsa#2674

### How to use

After setting it up, it will automatically show the current playercount on its Status. To access the other statistics, type /info.

### Backlog / Issues

[Akari Issues](https://gitlab.com/sinsa/akari/-/issues)

### Installation Guide

1. Install [Git](https://git-scm.com) and [NodeJS](https://nodejs.org/en/download/). Recommended is the latest LTS version.
2. Perform a `git clone https://gitlab.com/sinsa/akari.git` in the directory you wish the bot run in.
3. Run a `npm install` in the directory where the *package.json* is located.
4. Fill out the `config.json` (more info can be found in the config section).
5. Run `node index` to start the bot.

### Translations
By default, translations for English and German are provided by default.
To enable either of them, set the value `translation` in the config (defaults to `en`).
Use `en` for English and `de` for German.
If you want to add your own language, add a field in each text piece in the `translations.json` file.
Prefix it with your language shorthand (e.g. `fr` for French).
As soon as you've filled out all text pieces, you can use that language shorthand in the config now.

#### Contributing
If you made a translation for a language, I'll be happy to add it by default!
Simply create a merge request with your added translations.

### Config

| Config Value       | Explanation                                                                                                      | Example                                                                           | Default   | Required? |
| ------------------ | ---------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- | --------- | --------- |
| token              | The Bot-Token, obtained from https://discord.com/developers/applications.                                        | NwfzFsdfe542ed.4ztg3awg4F-Efahjgia43dsf.NERSser53                                 | none      | ✅        |
| srvgame            | The game the server is hosting. Should be `garrysmod` or `csgo` in most cases.                                   | garrysmod                                                                         | garrysmod | ✅        |
| srvip              | The IP of the server the bot should query.                                                                       | 1.1.1.1                                                                           | none      | ✅        |
| srvport            | The Port of the server the bot should query.                                                                     | 27015                                                                             | 27015     | ✅        |
| aboutdesc          | Some text that show up in the `about` command of the bot.                                                        | I'm responsible in bringing the TTT Statistics of our gameserver to this discord! | none      |           |
| srvcollection      | The ID of your Steam Workshop collection. Findable in the URL at the end.                                        | 2095342657                                                                        | none      |           |
| language           | The Language the bot should run in. You can add your own language by contributing to the translations.json file. | en                                                                                | en        | ✅        |
| commandOutputColor | The color of the embed which gets sent upon command usage. If left empty, it will pick a random color.           | #ffff00                                                                           | none      |           |
